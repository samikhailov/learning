package com.example

import java.util.*

fun isNumber(str: String) = try {
    str.toDouble()
    true
} catch (e: NumberFormatException) {
    false
}

/**
 * Возвращает строку с установленными пробелами после символов или чисел,
 * преобразованным унарным минусом и замененными запятыми на точки.
 */
fun prepareExpression(expression: String): String {
    var newExpression = if (expression.startsWith("-")) "(0-1)x${expression.substring(1)}" else expression
    newExpression = newExpression.replace(",", ".").replace(" ", "").replace("(-", "((0-1)x")
    val regex = Regex("\\(|\\)|[0-9]*[.]?[0-9]+|[+x/-]")
    return regex.replace(newExpression) {
        it.value + " "
    }.trim()
}


fun toReversePolishNotation(expression: String): String {
    val arrExpr = prepareExpression(expression).split(" ")
    val outputQueue = StringBuilder()
    val operatorStack = ArrayDeque<String>()

    arrExpr.forEach {
        when {
            isNumber(it) -> outputQueue.append("$it ")
            it == "+" || it == "-" -> {
                while (operatorStack.size > 0
                    && (operatorStack.peek().equals("x") || operatorStack.peek().equals("/")
                            || operatorStack.peek().equals("+") || operatorStack.peek().equals("-"))
                ) {
                    outputQueue.append("${operatorStack.pop()} ")
                }
                operatorStack.push(it)
            }
            it == "x" || it == "/" -> {
                while (operatorStack.size > 0
                    && (operatorStack.peek().equals("x") || operatorStack.peek().equals("/"))
                ) {
                    outputQueue.append("${operatorStack.pop()} ")
                }
                operatorStack.push(it)
            }
            it == "(" -> operatorStack.push(it)
            it == ")" -> {
                while (!operatorStack.peek().equals("(")) {
                    outputQueue.append("${operatorStack.pop()} ")
                }
                operatorStack.pop()
            }
            else -> return "Error"
        }
    }

    repeat(operatorStack.size) {
        outputQueue.append("${operatorStack.pop()} ")
    }
    return outputQueue.toString().trim()
}

fun calculate(expression: String): Float {
    val arrExpr = toReversePolishNotation(expression).split(" ")
    val stack = ArrayDeque<Float>()
    try {
        arrExpr.forEach {
            when {
                isNumber(it) -> stack.push(it.toFloat())
                it == "+" -> stack.push(stack.pop() + stack.pop())
                it == "-" -> stack.push(0 - stack.pop() + stack.pop())
                it == "x" -> stack.push(stack.pop() * stack.pop())
                it == "/" -> stack.push(1 / stack.pop() * stack.pop())
                else -> return Float.NaN
            }
        }
    } catch (e: NoSuchElementException) {
        return Float.NaN
    }

    return if (stack.size == 1 && !(stack.peek() == Float.POSITIVE_INFINITY || stack.peek() == Float.NEGATIVE_INFINITY)) stack.pop() else Float.NaN
}
