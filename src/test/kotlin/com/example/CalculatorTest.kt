package com.example

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CalculatorTest {
    @Test
    fun multiplyWithUnaryMinus() {
        val expression = "-3x5"
        val result = calculate(expression)
        assertEquals(
            -15f,
            result,
            "$expression evaluation error"
        )
    }

    @Test
    fun divisionWithUnaryMinus() {
        val expression = "-10/2"
        val result = calculate(expression)
        assertEquals(
            -5f,
            result,
            "$expression evaluation error"
        )
    }

    @Test
    fun additionWithUnaryMinus() {
        val expression = "-3+3"
        val result = calculate(expression)
        assertEquals(
            0f,
            result,
            "$expression evaluation error"
        )
    }

    @Test
    fun subtractionWithUnaryMinus() {
        val expression = "-3-5"
        val result = calculate(expression)
        assertEquals(
            -8f,
            result,
            "$expression evaluation error"
        )
    }

    @Test
    fun bracketWithUnaryMinus() {
        val expression = "-(-3-5)"
        val result = calculate(expression)
        assertEquals(
            8f,
            result,
            "$expression evaluation error"
        )
    }

    @Test
    fun nestedBracketsWithUnaryMinus() {
        val expression = "-(-(-5-10))"
        val result = calculate(expression)
        assertEquals(
            -15f,
            result,
            "$expression evaluation error"
        )
    }

    @Test
    fun crazySpaces() {
        val expression = "   ( 5 + 12-3    x4   )    /    5 - (10 - 10)"
        val result = calculate(expression)
        assertEquals(
            1f,
            result,
            "$expression evaluation error"
        )
    }

    @Test
    fun decimal() {
        val expression = "3,3 + 2 - 2.3"
        val result = calculate(expression)
        assertEquals(
            3f,
            result,
            0.000001f,
            "$expression evaluation error",
        )
    }

    @Test
    fun priorityOperations() {
        val expression = "(2 + 3) x 4 + (5 - 3) / 2"
        val result = calculate(expression)
        assertEquals(
            21f,
            result,
            "$expression evaluation error",
        )
    }

    @Test
    fun divisionByZero() {
        val expression = "1 / 0"
        val result = calculate(expression)
        assertEquals(
            Float.NaN,
            result,
            "$expression evaluation error",
        )
    }

    @Test
    fun incorrectExpression() {
        val expression = "4 - 3 +"
        val result = calculate(expression)
        assertEquals(
            Float.NaN,
            result,
            "$expression evaluation error",
        )
    }
}